const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'e-commerce API',
            description: 'e-commerce API',
            contact: {
                name: "Igna"
            },
            servers: ["http://localhost:3000"]
        }
    },

    apis: ["./routes/*.js"]
}

module.exports = swaggerOptions