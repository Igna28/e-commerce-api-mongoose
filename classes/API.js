const express = require('express');
const { response , request } = require('express');
const morgan = require('morgan')

const cors = require('cors');
const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express');

const { API_PORT } = require('../config');
const dbConnection = require('../database');

const _ = require("../locales")

class API {
    constructor(swaggerOptions){
        this.app = express();
        
        this.port = API_PORT;
        
        this.swaggerOptions = swaggerJsDoc(swaggerOptions)

        this.connectDatabase();

        this.middleWares();

        this.swagger();

        this.routes();
    }

    async connectDatabase(){
        await dbConnection()
    }

    middleWares(){
        this.app.use(cors());
        this.app.use(express.json());
        this.app.use(express.urlencoded({ 
            extended: true 
        }));

        if (process.env.NODE_ENV === "development") {
            this.app.use(morgan('combined'));
        }

        this.app.use('/api', require('../routes'))
    }

    swagger() {
        this.app.use("/swagger", swaggerUi.serve, swaggerUi.setup(this.swaggerOptions))
    }

    routes(){
        this.app.get("/", (pRequest, pResponse) => {
            pResponse.redirect("/swagger")
        })

        this.app.get("*", (pRequest, pResponse) => {
            pResponse.status(404).json({
                message: _("ROUTE_NO_FOUND")
            })
        })
    }

    listen(){
        this.app.listen(this.port, () => {
            console.log(`API -> Running http://localhost:${this.port}`)
        })
    }
}

module.exports = API;