const { Schema, model } = require('mongoose');

const Product = require('./Product')

const orderSchema = new Schema(
    {
        items: [{
            product: {
                type: Schema.Types.ObjectId,
                required: true,
                ref: 'Product',
                required: [true, "The 'product' field is required"],
            },
            quantity: {
                type: Number,
                required: true
            },
            price: {
                type: Number,
            },
            currency: {
                type: String,
            },
        }],
        user: {
            type: Schema.Types.ObjectId, 
            ref: 'User',
            required: [true, "The 'user' field is required"],
        },
        shippingPrice: {
            type: Number,
            required: true,
            default: 0.0
        },
        totalPrice: {
            type: Number,
            required: true,
            default: 0.0
        },
        shipping: {
            type: Object,
            required: [true, "The 'shipping' field is required"],
        },
        // payment: [{
        //     paymentMethod: {
        //         type: String,
        //         required: [true, "Please add a payment method"],
        //     },
        //     id: { 
        //         type: String 
        //     },
        //     status: { 
        //         type: String 
        //     },
        //     update_time: { 
        //         type: String 
        //     },
        //     email_address: { 
        //         type: String 
        //     },
        // }],
        status: {
            type: String,
            enum: [
                'pending',
                'failed',
                'paid',
                'shipped',
                'refunded',
                'cancelled',
                'on-hold',
            ],
            default: 'pending',
        },
    },
    {
        versionKey: false,
        timestamps: {
            createdAt: 'order_created',
            updatedAt: 'order_updated',
        },
    },
    {
        collection: 'orders'
    }
);

orderSchema.pre('save', async function(next) {   
    this.items = await Promise.all(this.items.map(async item => {
        const product = await Product.findById(item.product)

        if (!product) {
            next(err)
        }else{
            item.price = product.price
            item.currency = product.currency

            return item
        }
    }));

    const subtotal = this.items.reduce((subtotal, item) => {
        return subtotal + (item.price * item.quantity)
    }, 0).toFixed(2)
    
    const taxPrice = 1 + 10 / 100; // 10% taxes

    this.totalPrice = ((parseFloat(subtotal) + this.shippingPrice) * taxPrice).toFixed(2)

    next();
})

const Order = model('Order', orderSchema);

module.exports = Order;