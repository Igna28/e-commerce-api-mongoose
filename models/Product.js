const { Schema, model } = require('mongoose');

const productSchema = new Schema(
    {
        name: {
            type: String,
            required: [true, "The 'name' field is required"],            
            maxLength: [100, "Product 'name' cannot exceed 100 characters"]
        },
        description: {
            type: String,
            required: [true, "The 'description' field is required"],
            default: "No Description"
        },
        image_url: {
            type: String,
            default: "https://raw.githubusercontent.com/CondeReggi/Node/79110aa081655f981a02de01a09e86a3b196335c/RestServer/assets/no-image.jpg"   
        },
        released_at: {
            type: Date,
            default: Date.now()  
        },
        currency: {
            type: String,
            default: 'USD',
        },
        price: {
            type: Number,
            min: 0,
            default: 0.0
        },
        ratings: {
            type: Number,
            default: 0
        },
        category: {
            type: String,
            required: [true, 'Please select category for this product'],
            enum: {
                values: [
                    'Electronics',
                    'Cameras',
                    'Laptops',
                    'Accessories',
                    'Headphones',
                    'Food',
                    "Books",
                    'Clothes/Shoes',
                    'Beauty/Health',
                    'Sports',
                    'Outdoor',
                    'Home'
                ],
                message: 'Please select correct category for product'
            }
        },
        stock: {
            type: Number,
            required: [true, 'Please enter product stock'],
            maxLength: [5, 'Product name cannot exceed 5 characters'],
            default: 0
        },
        deleted: {
            type: Boolean,
            default: false,
            select: false
        }
    },
    {
        versionKey: false,
        timestamps: {
            createdAt: 'product_created',
            updatedAt: 'product_updated',
        },
    },
    {
        collection: 'products'
    }
);

productSchema.methods.getProductStock = function() {
    return this.stock
};

productSchema.methods.toJSON = function() {
    const { __v, deleted, ...product } = this.toObject();
    
    return product;
};

const Product = model('Product', productSchema);

module.exports = Product;
  