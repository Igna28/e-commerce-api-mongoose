const { Schema, model } = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");

const { TOKEN_SECRET } = require('../config')

const userSchema = new Schema(
    {
        email: {
            type: String,
            required: [true, "The 'email' field is required"],
            maxlength: 255,
            unique: true,
        },
        username: {
            type: String,
            required: [true, "The 'username' field is required"],
            maxlength: 255,
            unique: true
        },
        first_name: {
            type: String,
            maxlength: 255,
        },
        last_name: {
            type: String,
            maxlength: 255,
        },
        password: {
            type: String,
            required: [true, "The 'password' field is required"],
            minlength: 4,
            maxlength: 1024,
            select: false
        },
        payment_method: {
            paypal_account: {
                type: String,
                default: null
            },
            type: Object
        },
        image_url: {
            type: String,
            default: 'https://res.cloudinary.com/dj9edroyv/image/upload/v1607702436/Vaquita/lxvo7wzoriugvszkl2pj.png'
        },
        role: {
            type: String,
            default: "user"
        },
        deleted: {
            type: Boolean,
            default: false,
            select: false
        }
    },
    {
        versionKey: false,
        timestamps: {
            createdAt: 'user_created',
            updatedAt: 'user_created',
        },
    },
    {
        collection: 'users'
    }
);

userSchema.pre("save", function(next) {
    var user = this;

    if (!user.isModified('password')) return next();

    bcrypt.genSalt(10, function(err, salt) {
        if (err) return next(err);
    
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);
            user.password = hash;
            next();
        });
    });
})

userSchema.methods.comparePassword = function(candidatePassword, cb) {
    User.findOne({
        email: this.email
    }).select('password').exec(function (err, user) {
        bcrypt.compare(candidatePassword, user.password, function(err, isMatch) {
            if (err) return cb(err);
            cb(null, isMatch);
        });
    })
};

userSchema.methods.generateAuthToken = function() {
    return jwt.sign({
        userId: this._id,
        email: this.email,
        isAdmin: this.isAdmin
    }, TOKEN_SECRET, { 
        expiresIn: '2h' 
    });
};

const User = model('User', userSchema);

module.exports = User;
  