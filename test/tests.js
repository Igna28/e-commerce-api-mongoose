const chai = require('chai');
const chaiHttp = require('chai-http');
const api = require('../app');
const should = chai.should();

chai.use(chaiHttp);

const { API_PORT } = require('../config')

const User = require('../models/User')

const api_url = `http://localhost:${API_PORT}/api`

describe('Users', () => {
    beforeEach((done) => {
        User.deleteOne({}, (err) => {
           done();
        });
    });

    describe('/GET users', () => {
        it('it should GET all the users', (done) => {
            chai.request(api_url)
                .get('/users')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('data').which.is.an('array');
                    res.body.should.have.property('success').which.is.an('boolean');
                done();
            });
        });
    });
});

describe('Authentication', () => {
    describe('/POST authentication/register', () => {
        it('it should not REGISTER a user without user data', (done) => {
            const user = {
                email: "thomassnow@thomas.com",
                username: "thomassnow",
                first_name: "Thomas",
                last_name: "Snow",
                password: "1234"
            }
            
            chai.request(api_url)
                .post('/authentication/register')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                done();
            });
        });

        it('it should reject users with duplicate emails', (done) => {
            const user = {
                email: "thomassnow@thomas.com",
                password: "1234"
            }

            chai.request(api_url)
              .post('/authentication/register')
              .send(user)
              .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').which.is.an('boolean');
                    res.body.should.have.property('message').which.is.an('array');
                done()
            })
        })      

        it('it should reject an empty string password', (done) => {
            const user = {
                email: "thomassnow@thomas.com",
                password: ""
            }

            chai.request(api_url)
              .post('/authentication/register')
              .send(user)
              .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').which.is.an('boolean');
                    res.body.should.have.property('message').which.is.an('array');
                done()
            })
        })
    });

    describe('/POST authentication/login', () => {
        it('it should not LOGIN a user without user data', (done) => {
            const user = {
                email: "thomassnow@thomas.com",
                password: "1234"
            }
            chai.request(api_url)
                .post('/authentication/login')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('data').which.is.an('object');
                    res.body.should.have.property('success').which.is.an('boolean');
                    res.body.should.have.property('message').which.is.an('string');
                done();
            });
        });

        it('it should reject users with non-matching passwords', done => {
            const user = {
                email: "thomassnow@thomas.com",
                password: "12345"
            }

            chai.request(api_url)
                .post('/authentication/login')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('success').which.is.an('boolean');
                    res.body.should.have.property('message').which.is.an('string');
                done()
            })
        })
    });
});