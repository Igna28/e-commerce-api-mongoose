const { response , request } = require('express');
const jwt = require("jsonwebtoken");

const { TOKEN_SECRET } = require('../config')

const _ = require("../locales")

const { 
    unAuthorizedResponse
} = require('../helpers/responses')

const User = require('../models/User')
const Product = require('../models/Product');
const Order = require('../models/Order');

const verifyToken = async (req = request, res = response, next) => {
    const authHeader = req.headers['authorization']
    
    const token = authHeader && authHeader.split(' ')[1]

    if (token == null) {
        unAuthorizedResponse(res, {
            success: false,
            message: _("INVALID_TOKEN_NO_SEND")
        })
    }else{
        try{
            const { userId } = jwt.verify(token, TOKEN_SECRET);
            
            const user = await User.findById(userId);

            if (!user) {
                unAuthorizedResponse(res, {
                    success: false,
                    message: _("INVALID_TOKEN_NO_VALID")
                })
            }else{
                req.user = user;

                next();
            }
        }catch(error) {
            console.log(error);

            unAuthorizedResponse(res, {
                success: false,
                message: error.message
            })
        }
    }
}

const checkUserRole = (pRole) => async (req, res, next) => {
    if (pRole == req.user.role) {
        next();
     
    }else{
        res.status(401).json({
            success: false,
            message: `User role '${req.user.role}' is not allowed to access this resource`
        })
    }
}

const checkEmailIfExists = async (pEmail) => {
    const user = await User.findOne({ 
        email: pEmail,
        deleted: false
    })

    if (user) {
        throw new Error(`The email already exist.`)
    }
}

const checkUserIfExists = async (pId) => {
    const user = await User.findOne({ 
        _id: pId,
        deleted: false
    }) 

    if (!user) {
        throw new Error(`The user doesn't exist.`)
    }
}

const checkProductIfExists = async (pProductId) => {
    const product = await Product.findOne({ 
        _id: pProductId,
        deleted: false
    })

    if (!product) {
        throw new Error(`The product doesn't exist.`)
    }
}

const checkOrderIfExists = async (pOrderId) => {
    const order = await Order.findOne({ 
        _id: pOrderId,
        deleted: false
    }) 

    if (!order) {
        throw new Error(`The order doesn't exist.`)
    }
}

module.exports = {
    verifyToken,
    checkUserRole,
    checkEmailIfExists,
    checkUserIfExists,
    checkProductIfExists,
    checkOrderIfExists
};