const authentication = require('./authentication');
const fieldsValidator = require('./fieldsValidator');

module.exports = {
    ...authentication,
    ...fieldsValidator
}