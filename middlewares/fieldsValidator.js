const { validationResult } = require("express-validator");

const fieldValidator = (req, res, next) => {
    const validationErrors  = validationResult(req);

    try {
        if (!validationErrors.isEmpty()) {
            const errors = validationErrors.array().map((error) => `${error.msg}`);
    
            res.status(400).json({
                success: false,
                message: errors
            });
        }else{
            next();
        }
    }catch (error) {
        console.log(`Middleware Validation error ${error}`);
    }
};

module.exports = {
    fieldValidator
};