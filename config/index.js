require('dotenv').config({ 
    path: `${__dirname}/.env` 
});

const NODE_ENV = process.env.NODE_ENV || 'development'

var DB_HOST = process.env.DB_HOST_DEVELOPMENT

if (NODE_ENV === 'production') {
    DB_HOST = process.env.DB_HOST_PRODUCTION
}else if (NODE_ENV === 'test') {
    DB_HOST = process.env.DB_HOST_TEST
}

module.exports = {
    NODE_ENV,

    API_PORT: process.env.API_PORT || 3000,
    TOKEN_SECRET: process.env.TOKEN_SECRET,
    DB_HOST: DB_HOST,
    
    LANGUAGUE_CODE: process.env.LANGUAGUE
}

