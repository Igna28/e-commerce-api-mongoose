# e-Commerce API #

This is a NodeJS application (e-Commerce backend) for development concepts as HTTP requests and using middlewares, utilizes Express Web Framework and Mongoose to model and connect to a MongoDB database and bunch of others dependencies listed above.

## Installation
* Clone the repository
* Execute `npm install`

## Configuration
The configuration for the API is in the `config` folder in the `.env` file and in the `index.js`.

* Create a .env file in config folder and add the following:

```
NODE_ENV = development

API_PORT = 3000
TOKEN_SECRET = ADD_YOUR_TOKEN_HERE

DB_HOST_DEVELOPMENT = mongodb://127.0.0.1:27017/ecommerce-development
DB_HOST_PRODUCTION = mongodb://127.0.0.1:27017/ecommerce-production
DB_HOST_TEST = mongodb://127.0.0.1:27017/ecommerce-test

LANGUAGUE = "en-US"
```

### Locales System
This API was build with a simple custom locales system, so you can change the response messages or add new languages as you like, without touching the code.
You can add or modify languages in the `locales` folder in the `languagues.json` file. The default language is American English (en-US).

### Seed Data
* Insert data : `node ./seeds/seeds -i`
* Remove data : `node ./seeds/seeds -d`

Note: Is intended to use only on the development database.

## Start API
To initialize the API you have to execute these bellow commands, depends of your work mode:

* For **Development:** `npm run dev`
* For **Production:** `npm run prod`
* For **Testing:** `npm run test`

## API Routes

### Authentication

Method | URI                          | Status          | Description                                | Protected                     |
------ | ---------------------------- | --------------- | ------------------------------------------ | ----------------------------- |
POST   | /api/authentication/register | 200, Success    | Register user                              | -                             |
POST   | /authentication/login        | 200, Success    | Login user, return user data and jwt token | -                             |

### Users

Method  | URI                          | Status          | Description                   | Protected                        |
------- | ---------------------------- | --------------- | ----------------------------- | -------------------------------- |
GET     | /api/users                   | 200, Success    | Return all products           | -                                |
GET     | /api/users/{id}              | 200, Success    | Return a product              | -                                |
DELETE  | /api/users/{id}              | 200, Success    | Delete a product              | JWT Authentication  + Admin role |
GET     | /api/users/{id}/orders       | 200, Success    | Return all users orders       | -                                |
POST    | /api/users/{id}/createOrder  | 200, Success    | Create a new order for a user | JWT Authentication               | 

### Products

Method  | URI                    | Status          | Description                   | Protected                        |
------- | ---------------------- | --------------- | ----------------------------- | -------------------------------- |
GET     | /api/products          | 200, Success    | Return all products           | -                                |
GET     | /api/products/{id}     | 200, Success    | Return a product              | -                                |
POST    | /api/products          | 200, Success    | Create a new product          | JWT Authentication  + Admin role |
DELETE  | /api/products/{id}     | 200, Success    | Delete a product              | JWT Authentication  + Admin role |

### Orders

Method  | URI               | Status          | Description       | Protected |
------- | ----------------- | --------------- | ----------------- | --------- |
GET     | /api/orders       | 200, Success    | Return all orders | -         |
GET     | /api/orders/{id}  | 200, Success    | Return a product  | -         |

## TODO

* Add more endpoints testing.
* Create a custom documentation to replace swagger.
* Support categories on locales system. For example: 
````javascript
{
    "en-us": {
        "general": {
            "GENERAL_LOCALE": "..."
        },
        "routes": {
            "ROUTE_LOCALE": "..."
        }
   }
}
````
* Build a frontend for using some framework (React, Vue etc) and add payment backend. 
* Add payment support.
* More endpoints to allow more functions.

## Dependencies
* [Node.js](https://github.com/nodejs/node) - v16.14.0.

### Web Framework
* [Express](https://expressjs.com/) - v4.17.1
* [Cors](https://github.com/expressjs/cors) - v2.8.5

### Configuration
* [dotenv](https://github.com/motdotla/dotenv) - v10.0.0

### Authentication
* [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken) - v8.5.1
* [bcryptjs](https://github.com/dcodeIO/bcrypt.js) - v2.4.3

### Validations
* [express-validator](https://express-validator.github.io/docs/) - v6.14.0

### Database
* [mongoose](https://github.com/Automattic/mongoose) - v6.1.4

### Documentation
* [swagger-jsdoc](https://github.com/Surnet/swagger-jsdoc) - v6.1.0
* [swagger-ui-express](https://github.com/scottie1984/swagger-ui-express) - v4.2.0

### Development Tools
* [morgan](https://github.com/expressjs/morgan) - v1.10.0
* [nodemon](https://github.com/remy/nodemon) - v2.0.15

### Testing
* [mocha](https://mochajs.org/) - v9.2.0
* [chai](https://www.chaijs.com/) - v4.3.6
* [chai-http](https://www.chaijs.com/plugins/chai-http/) - v4.3.0