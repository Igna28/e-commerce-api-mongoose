const router = require('express').Router()

const users = require('./users')
const products = require('./products')
const authentication = require('./authentication')
const orders = require('./orders')

router.use('/authentication', authentication)
router.use('/users', users)
router.use('/products', products)
router.use('/orders', orders)

module.exports = router