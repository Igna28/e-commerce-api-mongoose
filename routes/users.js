const router = require('express').Router()
const { check } = require('express-validator');

const { 
  verifyToken, 
  fieldValidator,
  checkUserIfExists,
  checkUserRole,
} = require('../middlewares')

const { 
  userGet, 
  userFindById, 
  userDeleteById, 
  userOrders,
} = require('../controllers')

/**
  * @swagger
  * tags:
  *   name: Users
  *   description: The Users managing API
*/

/**
  * @swagger
  * /api/users:
  *  get:
  *    summary: Use to request all users
  *    tags: [Users]
  *    responses:
  *      '200':
  *        description: A successful response
 */
router.get('/', userGet);

/**
  * @swagger
  * /api/users/{id}:
  *  get:
  *    summary: Use to request all users
  *    tags: [Users]
  *    parameters:
  *      -   in: path
  *          name: id
  *          description: Id of the user
  *          required: true
  *          schema:
  *              type: string
  *              format: string
  *    responses:
  *      '200':
  *        description: A successful response
*/
router.get('/:id', [
    check('id', 'The id is not valid').isMongoId(),
    check('id').custom(checkUserIfExists),
    fieldValidator
], userFindById);

/**
  * @swagger
  * /api/users/{id}:
  *  delete:
  *    summary: Remove a user by Id
  *    tags: [Users]
  *    parameters:
  *      -   in: path
  *          name: id
  *          description: Id of the user
  *          required: true
  *          schema:
  *              type: string
  *              format: string
  *    responses:
  *      '200':
  *        description: A successful response
*/
router.delete('/:id', [
    verifyToken,
    checkUserRole("admin"),
    check('id', "The id is not mongoId valid.").isMongoId(),
    check("id", "The 'id' field can't be empty.").not().isEmpty(),
    check('id').custom(checkUserIfExists),
    fieldValidator
], userDeleteById);

/**
  * @swagger
  * /api/users/{id}/orders:
  *  get:
  *    summary: Get all user orders
  *    tags: [Users]
  *    parameters:
  *      -   in: path
  *          name: id
  *          description: Id of the user
  *          required: true
  *          schema:
  *              type: string
  *              format: string
  *    responses:
  *      '200':
  *        description: A successful response
*/
router.get('/:id/orders', [
    check('id', "The id is not mongoId valid.").isMongoId(),
    check("id", "The 'id' field can't be empty.").not().isEmpty(),
    check('id').custom(checkUserIfExists),
    fieldValidator
], userOrders);

module.exports = router