const router = require('express').Router();
const { check } = require('express-validator');

const { 
    getOrders,
    ordersFindById,
    createOrder
} = require('../controllers');

const { 
  checkOrderIfExists,
  checkUserIfExists
} = require('../middlewares');

const { verifyToken, fieldValidator } = require('../middlewares')

/**
  * @swagger
  * tags:
  *   name: Orders
  *   description: The Orders managing API
*/

/**
  * @swagger
  * /api/orders:
  *  get:
  *    summary: Get all orders data
  *    tags: [Orders]
  *    responses:
  *      '200':
  *        description: A successful response
*/
router.get('/', getOrders)
   
/**
  * @swagger
  * /api/orders/{id}:
  *  get:
  *    summary: Get order by id data
  *    tags: [Orders]
  *    parameters:
  *      -   in: path
  *          name: id
  *          description: Id of the Product
  *          required: true
  *          schema:
  *              type: string
  *              format: string
  *    responses:
  *      '200':
  *        description: A successful response
*/
router.get('/:id', [
    check('id', 'The id is not valid').isMongoId(),
    check('id').custom(checkOrderIfExists),
    fieldValidator
], ordersFindById);

/**
  * @swagger
  * /api/orders/user/{id}:
  *  post:
  *    summary: Create a new order.
  *    tags: [Orders]
  *    parameters:
  *      -   in: path
  *          name: id
  *          description: Id of the user
  *          required: true
  *          schema:
  *              type: string
  *              format: string
  *      -   in: body
  *          name: product
  *          description: The Order to create.
  *          required: true
  *          schema:
  *              required:
  *                  - items
  *                  - shippingPrice
  *                  - shipping
  *              properties:
  *                  items:
  *                    type: array
  *                  shippingPrice: 
  *                    type: integer
  *                  shipping:
  *                    type: object
  *              example: {
  *                      "items": [
  *                            {
  *                                "product": "620e7ed461fae374117a3b33",
  *                                "quantity": 1
  *                            }
  *                        ],
  *                      "shippingPrice": 5,
  *                      "shipping": {
  *                        "first_name": "Thomas",
  *                        "last_name": "Snow",
  *                        "company": "",
  *                        "address_1": "PO.Box 10500",
  *                        "address_2": "",
  *                        "city": "Los Angeles",
  *                        "postal_code": "90001",
  *                        "country": "United States",
  *                        "state": "California",
  *                        "preferred_method": "3Day",
  *                        "phone": "904-759-8019"
  *                      },
  *                    }
  *    responses:
  *      '200':
  *        description: A successful response
*/
router.post('/user/:id', [
  verifyToken,
  check('id', "The 'id' field is not mongoId valid.").isMongoId(),
  check("id", "The 'id' field is required.").not().isEmpty(),
  check('id').custom(checkUserIfExists),
  fieldValidator
], createOrder);

module.exports = router