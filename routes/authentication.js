const router = require('express').Router()
const { check } = require('express-validator');

const { 
    register, 
    login 
} = require('../controllers');

const { 
    fieldValidator,
    checkEmailIfExists
} = require('../middlewares')

/**
  * @swagger
  * tags:
  *   name: Authentication
  *   description: The Authentication managing API
*/

/**
  * @swagger
  * /api/authentication/register:
  *  post:
  *    summary: Register a new user.
  *    tags: [Authentication]
  *    parameters:
  *      -   in: body
  *          name: user
  *          description: The user to create.
  *          required: true
  *          schema:
  *              required:
  *                  - email
  *                  - username
  *                  - first_name
  *                  - last_name
  *                  - password
  *                  - image_url
  *              properties:
  *                  email:
  *                    type: string
  *                  username:
  *                    type: string
  *                  first_name:
  *                    type: string
  *                  last_name:
  *                    type: string
  *                  password:
  *                    type: password
  *                  payment_option:
  *                    type: array
  *              example: {
  *                 "email": "thomassnow@thomas.com",
  *                 "username": "thomassnow",
  *                 "first_name": "Thomas",
  *                 "last_name": "Snow",
  *                 "password": "1234",
  *                 "payment_option": {
  *                    paypal_account: "thomassnow_payment@thomas.com"          
  *                  },
  *                 "image_url": ""
  *              }
  *    responses:
  *      '200':
  *        description: A successful response
*/
router.post('/register', [
    check('email', "The 'email' field isn't a valid email format.").not().isEmpty().isEmail(),
    check('username', "The 'username' field is required.").not().isEmpty(),
    check('first_name', "The 'first_name' field is required.").not().isEmpty(),
    check('last_name', "The 'last_name' field is required.").not().isEmpty(),
    check('password', "The 'password' field cannot be empty.").not().isEmpty(),
    check('password', "The 'password' field cannot be less than four characters").isLength({ 
        min: 4
    }),
    check('email', "The 'email' already exist." ).custom(checkEmailIfExists),
    fieldValidator
], register);

/**
 * @swagger
 * /api/authentication/login:
 *  post:
 *    summary: Login user.
 *    tags: [Authentication]
 *    parameters:
 *      -   in: body
 *          name: user
 *          description: The user to authenticate
 *          required: true
 *          schema:
 *              required:
 *                  - email
 *                  - password
 *              properties:
 *                  email:
 *                    type: string
 *                  password:
 *                    type: password
 *              example: {
 *                 "email": "thomassnow@thomas.com",
 *                 "password": "1234"
 *              }
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.post('/login', [
    check('email', "The 'email' field is empty.").not().isEmpty(),
    check('email', "The 'email' field isn't a valid email format.").isEmail(),
    check('password', "The 'password' field cannot be empty.").not().isEmpty(),
    check('password', "The 'password' field cannot be less than four characters.").isLength({ 
        min: 4
    }),
    fieldValidator
], login);

module.exports = router