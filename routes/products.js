const router = require('express').Router()
const { check } = require('express-validator');

const { 
    productsGet,
    productFindById,
    productPost,
    productDeleteById 
} = require('../controllers')

const { 
  checkUserRole,
  checkProductIfExists
} = require('../middlewares')

const { verifyToken, fieldValidator } = require('../middlewares')

/**
  * @swagger
  * tags:
  *   name: Products
  *   description: The Product managing API
*/

/**
  * @swagger
  * /api/products:
  *  get:
  *    summary: Use to request all Products
  *    tags: [Products]
  *    responses:
  *      '200':
  *        description: A successful response
*/
router.get('/', productsGet)

 /**
  * @swagger
  * /api/products/{id}:
  *  get:
  *    summary: Use to request all products
  *    tags: [Products]
  *    parameters:
  *      -   in: path
  *          name: id
  *          description: Id of the Product
  *          required: true
  *          schema:
  *              type: string
  *              format: string
  *    responses:
  *      '200':
  *        description: A successful response
*/
router.get('/:id', [
    check('id', 'The id is not valid').isMongoId(),
    check('id').custom(checkProductIfExists),
    fieldValidator
], productFindById);

 /**
  * @swagger
  * /api/products:
  *  post:
  *    summary: Creates a new product.
  *    tags: [Products]
  *    parameters:
  *      -   in: body
  *          name: product
  *          description: The product to create.
  *          required: true
  *          schema:
  *              required:
  *                  - name
  *                  - description
  *                  - image_url
  *                  - released_at
  *                  - price
  *                  - rating
  *                  - category
  *                  - stock
  *              properties:
  *                  name:
  *                    type: string
  *                  description:
  *                    type: string
  *                  image_url:
  *                    type: string
  *                  released_at:
  *                    type: string
  *                  currency:
  *                    type: string
  *                  price:
  *                    type: integer
  *                  rating:
  *                    type: integer
  *                  category:
  *                    type: string
  *                  stock:
  *                    type: integer
  *              example: {
  *                 "name": "Iphone 11",
  *                 "description": "Graba videos 4K y captura retratos espectaculares y paisajes increíbles con el sistema de dos cámaras. Toma grandes fotos con poca luz gracias al modo Noche. Disfruta colores reales en las fotos, videos y juegos con la pantalla Liquid Retina de 6.1 pulgadas (3). Aprovecha un rendimiento sin precedentes en los juegos, la realidad aumentada y la fotografía con el chip A13 Bionic. Haz mucho más sin necesidad de volver a cargar el teléfono gracias a su batería de larga duración (2). Y no te preocupes si se moja, el iPhone 11 tiene una resistencia al agua de hasta 30 minutos a una profundidad máxima de 2 metros (1).",
  *                 "image_url": "https://http2.mlstatic.com/D_NQ_NP_672533-MLA46153369213_052021-V.webp",
  *                 "released_at": "09/20/2019",
  *                 "currency": "USD",
  *                 "price": 1000,
  *                 "rating": 5,
  *                 "category": "Electronics",
  *                 "stock": 1
  *              }
  *    responses:
  *      '200':
  *        description: A successful response
*/
router.post('/', [
    verifyToken,
    checkUserRole("admin"),
    check("name", "The field 'name' is required").not().isEmpty(),
    check("description", "The field 'description' is required").not().isEmpty(),
    check("price", "The field 'price' is required").not().isEmpty(),
    fieldValidator
], productPost);

 /**
  * @swagger
  * /api/products/{id}:
  *  delete:
  *    summary: Remove a product by Id
  *    tags: [Products]
  *    parameters:
  *      -   in: path
  *          name: id
  *          description: Id of the product
  *          required: true
  *          schema:
  *              type: string
  *              format: string
  *    responses:
  *      '200':
  *        description: A successful response
*/
router.delete('/:id', [
    verifyToken,
    checkUserRole("admin"),
    check('id', 'The id is not valid').isMongoId(),
    check('id').custom(checkProductIfExists),
    fieldValidator
], productDeleteById);


module.exports = router