// SuccessResponse 200
// ErrorResponse 500
// NotFoundResponse 404
// UnauthorizedResponse 401
// BadRequestResponse 422

const successResponse = (pResponse, pJsonMessage) => pResponse.status(200).json(pJsonMessage);
const errorResponse = (pResponse, pJsonMessage) => pResponse.status(500).json(pJsonMessage);
const notFoundResponse = (pResponse, pJsonMessage) => pResponse.status(404).json(pJsonMessage);
const badRequestResponse = (pResponse, pJsonMessage) => pResponse.status(400).json(pJsonMessage);
const unAuthorizedResponse = (pResponse, pJsonMessage) => pResponse.status(401).json(pJsonMessage);

module.exports = {
    successResponse,
    errorResponse,
    notFoundResponse,
    badRequestResponse,
    unAuthorizedResponse
}