const mongoose = require('mongoose')

const { DB_HOST } = require('../config')

const dbConnection = async() => {
    try {
        await mongoose.connect(DB_HOST, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });

        console.log(`Database -> Running ${DB_HOST}`)
    }catch (error) {
        console.log(error)
        
        throw new Error('Database -> It was an error trying to initialize the database.')
    }
}

module.exports = dbConnection