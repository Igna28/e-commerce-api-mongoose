const fs = require("fs");
const dbConnection  = require('../database')

const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

const users = JSON.parse(
    fs.readFileSync(`${__dirname}/data/users.json`, "utf-8")
);

const products = JSON.parse(
    fs.readFileSync(`${__dirname}/data/products.json`, "utf-8")
);

const orders = JSON.parse(
    fs.readFileSync(`${__dirname}/data/orders.json`, "utf-8")
);

const importData = async () => {
    await dbConnection();

    try {
        await User.insertMany(users);
        await Product.create(products);
        await Order.create(orders);

        console.log(`Seeds -> Data Imported`);
        
        process.exit();
    }catch(error) {
        console.log(error);
    }
};

const deleteData = async () => {
    await dbConnection();

    try {
        await User.deleteMany();

        await Product.deleteMany();

        await Order.deleteMany();

        console.log(`Seeds -> Data Destroyed`);

        process.exit();
    }catch(error) {
        console.log(error);
    }
};

if (process.argv[2] === "-i") {
    importData();
} else if (process.argv[2] === "-d") {
    deleteData();
}