const { LANGUAGUE_CODE } = require('../config')
const locales = require('./languagues.json')

const _ = (pLocaleName) => {
    const languagueCode = LANGUAGUE_CODE || "en-US"

    if (locales) {
        if (locales[languagueCode]) {
            if (locales[languagueCode][pLocaleName]) {
                return locales[languagueCode][pLocaleName]
            }else{
                console.log("Missing locale (" + pLocaleName + ")")

                return "Missing locale (" + pLocaleName + ")"
            }

        }else{
            console.log("Missing language (" + languagueCode + ")")

            return "Missing language (" + languagueCode + ")"
        }
    }
}


module.exports = _
