require('./config')

const swaggerOptions = require('./swagger/swagger');

const Api = require('./classes/API'); 

const api = new Api(swaggerOptions);

api.listen();

module.exports = api.app