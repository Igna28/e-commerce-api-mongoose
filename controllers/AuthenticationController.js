const { response, request } = require("express");

const _ = require("../locales")

const { 
    successResponse,
    errorResponse,
    notFoundResponse
} = require('../helpers/responses')

const User = require('../models/User')

const register = async (req = request, res = response) => {
    const { 
        email, 
        username,
        first_name,
        last_name,
        password,
        payment_option,
        image_url,
    } = req.body
	
    const newUser = new User({
		email: email,
		username: username,
        first_name: first_name,
        last_name: last_name,
		password: password,
        payment_method: payment_option,
        image_url: image_url,
    })

	try {
        await newUser.save()

		successResponse(res, {
			success: true,
            message: _("NEW_USER_CREATED")
		})
    }catch(error) {
        console.log(error);

        errorResponse(res, {
            success: false,
            message: error.message
        })
    }
}

const login = async (req = request, res = response) => {
    const { 
        email, 
        password 
    } = req.body

    try {
        const user = await User.findOne({
            email: email
        })   
        
        if (user) {
            user.comparePassword(password, function(pError, pIsMatch) {
                if (pError) throw pError;

                if (pIsMatch) {
                    const token = user.generateAuthToken();

                    successResponse(res, {
                        data: {
                            user: {
                                username: user.username,
                                isAdmin: user.isAdmin
                            },
                            access_token: token
                        },
                        success: true,
                        message: _("LOGIN_SUCCESSFUL")
                    })      
                }else{
                    successResponse(res, {
                        success: false,
                        message: _('PASSWORD_ERROR')
                    })  
                } 
            });        
        }else{
            notFoundResponse(res, {
                success: false,
                message: _('USER_NOT_FOUND')
            })
        }
    }catch(error){
        console.log(error)

        errorResponse(res, {
            success: false,
            message: error.message
        })
    }
}

module.exports = {
    register,
    login
}