const { response, request } = require("express");

const _ = require("../locales")

const { 
    successResponse,
    errorResponse,
    notFoundResponse
} = require('../helpers/responses')

const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');

const getOrders = async (req = request, res = response) => {
    try {
        const orders = await Order.find()
            .populate("user", {
                email : 1,
                username : 1
            })
            .populate("items.product", {
                price: 0,
                stock: 0
            })
    
        successResponse(res, {
            data: orders,
            success: true,
        })
    }catch(error) {
        console.log(error);

        errorResponse(res, {
            success: false,
            message: error.message
        })
    }
}

const ordersFindById = async (req = request, res = response) => {
    const { id } = req.params

    try {
        const order = await Order.findById(id)
            .populate("items.product", {
                description : 1,
                image_url : 1
            })
            .populate("user", {
                email : 1,
                username : 1
            });

        if (order) {
            successResponse(res, {
                data: order,
                success: true,
            });
        }else{
            notFoundResponse(res, {
                success: false,
                message: _("ORDER_NOT_FOUND")
            });
        }
    }catch(error) {
        console.log(error);

        errorResponse(res, {
            success: false,
            message: error.message
        });
    }
}

const createOrder = async (req = request, res = response) => {
    const { 
        id
    } = req.params

    const user = await User.findById(id);

    const newOrder = new Order({
        items: req.body.items,
        user: user._id,
        shippingPrice: req.body.shippingPrice,
        shipping: req.body.shipping
    });

    var hasStock = true;

    await Promise.all(newOrder.items.map(async item => {
        const product = await Product.findById(item.product);

        if (product) {
            const stock = product.getProductStock();

            if ((stock <= 0) && (hasStock)) {
                hasStock = false
            }      
        }
    }));

    if (hasStock) {
        newOrder.items = newOrder.items.map(item => {
            return {
                product: item.product,
                quantity: item.quantity        
            }
        })

        try {
            const savedOrder = await newOrder.save()

            successResponse(res, {
                success: false,
                message: "The order was created."
            });
        }catch(error) {
            console.log(error);

            errorResponse(res, {
                success: false,
                message: error.message
            })
        }
    }else{
        errorResponse(res, {
            success: false,
            message: "There is not stock available for one of the selected products."
        })
    }
}

module.exports = {
    getOrders,
    ordersFindById,
    createOrder
}