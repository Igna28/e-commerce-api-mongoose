const { response, request } = require("express");

const _ = require("../locales")

const { 
    successResponse,
    errorResponse,
    notFoundResponse
} = require('../helpers/responses')

const Product = require('../models/Product')

const productsGet = async (req = request, res = response) => {
    try {
        const products = await Product.find();
        
        successResponse(res, {
            data: products,
            success: true,
        })
    }catch (error){
        errorResponse(res, {
            success: false,
            message: error.message
        })
    }
}

const productFindById = async (req = request, res = response) => {
    const { id } = req.params

    try {
        const product = await Product.findOne({ 
            _id: id 
        })

        if (product) {
            successResponse(res, {
                message: product,
                success: true,
            })
        }else{
            notFoundResponse(res, {
                success: false,
                message: _("PRODUCT_NOT_FOUND")
            }) 
        }
    }catch (error){
        errorResponse(res, {
            success: false,
            message: error.message
        })
    }
}

const productPost = async (req = request, res = response) => {
    const { 
        name,
        description,
        image_url,
        released_at,
        price,
        rating,
        category,
        stock
    } = req.body

    const product = new Product({
        name: name,
        description: description,
        image_url: image_url,
        released_at: released_at,
        price: price,
        rating: rating,
        category: category,
        stock: stock
    })

    await product.save();

    successResponse(res, {
        message: _("PRODUCT_CREATED"),
        success: true,
    })
}

const productDeleteById = async (req = request, res = response) => {
    const { id } = req.params

    try {
        const user = await Product.findByIdAndUpdate(id, { 
            deleted: true 
        })

        if (user) {
            successResponse(res, {
                success: true,
                message: _("PRODUCT_DELETED")
            })
        }else{
            notFoundResponse(res, {
                success: false,
                message: _("PRODUCT_NOT_FOUND")
            })
        }
    }catch(error){
        errorResponse(res, {
            success: false,
            message: error.message
        })
    }
}

module.exports = {
    productsGet,
    productPost,
    productFindById,
    productDeleteById
}