const { response, request } = require("express");

const _ = require("../locales")

const { 
    successResponse,
    errorResponse,
    notFoundResponse
} = require('../helpers/responses')

const User = require('../models/User')
const Order = require('../models/Order')
const Product = require('../models/Product')

const userGet = async (req = request, res = response) => {
    try {
        const users = await User.find()

        successResponse(res, {
            data: users,
            success: true,
        })
    }catch(error){
        errorResponse(res, {
            success: false,
            message: error.message
        })
    }
}

const userFindById = async (req = request, res = response) => {
    const { id } = req.params

    try {
        const user = await User.findOne({ 
            _id: id
        })

        if (user) {
            successResponse(res, {
                data: user,
                success: true
            })
        }else{
            notFoundResponse(res, {
                success: false,
                message: _("USER_NOT_FOUND")
            })
        }
    }catch(error){
       errorResponse(res, {
            success: false,
            message: error.message
        })
    }
}

const userDeleteById = async (req = request, res = response) => {
    const { id } = req.params

    try {
        const user = await User.findByIdAndUpdate(id, { 
            deleted: true 
        })

        if (user) {
            successResponse(res, {
                success: true,
                message: _("USER_DELETED_SUCCESSFUL")
            })
        }else{
            notFoundResponse(res, {
                success: false,
                message: _("USER_NOT_FOUND")
            })
        }
    }catch(error){
       errorResponse(res, {
            success: false,
            message: error.message
        })
    }
}

const userOrders = async (req = request, res = response) => {
    const { id } = req.params

    try {
        const user = await User.findOne({ 
            _id: id
        })

        if (user) {
            const order = await Order.find({
                user: id
            })
            .populate("user", {
                _id: 0,
                email : 1,
                username : 1
            })
            .populate("items.product")
    
            successResponse(res, {
                data: order,
                success: true
            });
        }else{
            notFoundResponse(res, {
                success: false,
                message: _("USER_NOT_FOUND")
            })
        }
    }catch(error) {
        console.log(error);

        errorResponse(res, {
            success: false,
            message: error.message
        })
    }
}

module.exports = {
    userGet,
    userFindById,
    userDeleteById,
    userOrders
}