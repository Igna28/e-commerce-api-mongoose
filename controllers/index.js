const authenticationController = require('./AuthenticationController');
const userController = require('./UsersController');
const productsController = require('./ProductsController');
const ordersController = require('./OrdersController');

module.exports = {
    ...authenticationController,
    ...userController,
    ...productsController,
    ...ordersController
}